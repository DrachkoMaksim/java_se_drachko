package rpn.rpn;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Calcul {
	
	private HashMap<String, Integer> priority = new HashMap<>();
	
    public Calcul() {
    	priority.put("^", 3);
    	priority.put("*", 2);
    	priority.put("/", 2);
    	priority.put("+", 1);
    	priority.put("-", 1);
    }
	
	public Queue<String> infixToPostfix(Queue<String> infixQueue) {
		Stack<String> operatorStack = new Stack<>();
        Queue<String> postQueue = new LinkedList<>();
        String t;
        while (!infixQueue.isEmpty())
        {
            t = infixQueue.poll();
            try
            {
                double num = Double.parseDouble(t);
                postQueue.add(t);
            } catch (NumberFormatException nfe)
            {
                if (operatorStack.isEmpty())
                {
                    operatorStack.push(t);
                } else if (t.equals("("))
                {
                    operatorStack.push(t);
                } else if (t.equals(")"))
                {
                    while (!operatorStack.peek().equals("("))
                    {
                        postQueue.add(operatorStack.pop());
                    }
                    operatorStack.pop();
                } else
                {
                    while (!operatorStack.empty() && !operatorStack.peek().equals("(") && priority.get(t) <= priority.get(operatorStack.peek()))
                    {
                        postQueue.add(operatorStack.pop());                        
                    }
                    operatorStack.push(t);
                }
            }
        }
        while (!operatorStack.empty())
        {
            postQueue.add(operatorStack.pop());
        }
		return postQueue;
	}
	
	public double calculatePostfix(Queue<String> postQueue) {
		
		Stack<Double> res = new Stack<>();
		String t;
		double op1, op2;
		
		while (!postQueue.isEmpty())
        {
            t = postQueue.poll();
            try
            {
                double num = Double.parseDouble(t);
                res.push(num);
            } catch (NumberFormatException nfe)
            {
            	op1 = res.pop();
            	op2 = res.pop();
            	
            	switch(t) {
            	case"+":
            		res.push(op2+op1);
            		break;
            	case"-":
            		res.push(op2-op1);
            		break;
            	case"*":
            		res.push(op2*op1);
            		break;
            	case"/":
            		res.push(op2/op1);
            		break;
            	case"^":
            		res.push(Math.pow(op2, op1));
            		break;
            	}
            }
        }
		
        return res.pop();
	}
}
