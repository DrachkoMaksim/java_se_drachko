package rpn.rpn;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class App 
{
    public static void main( String[] args )
    {
    	Queue<String> inf = new LinkedList<>();
    	inf.add("1");
    	inf.add("+");
    	inf.add("2");
    	inf.add("*");
    	inf.add("3");
    	
    	Calcul rpn = new Calcul();
    	
    	Queue<String> post = rpn.infixToPostfix(inf);
    	
    	for(String s : post) {
    		System.out.println(s);
    	}
    	
    	System.out.println(rpn.calculatePostfix(post));
    }
}
