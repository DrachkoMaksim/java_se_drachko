package rpn.rpn;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Queue;

public class Test_RPN {
	@Test
	public void post_fix() {
		Queue<String> pattern = new LinkedList<>();
		pattern.add("(");
		pattern.add("30");
		pattern.add("-");
		pattern.add("15");
		pattern.add(")");
		pattern.add("/");
		pattern.add("(");
		pattern.add("5");
		pattern.add("+");
		pattern.add("2");
		pattern.add(")");
		
		Queue<String> expected = new LinkedList<>();
		expected.add("30");
		expected.add("15");
		expected.add("-");
		expected.add("5");
		expected.add("2");
		expected.add("+");
		expected.add("/");
		
		Calcul rpn = new Calcul();
		
		assertEquals(expected, rpn.infixToPostfix(pattern));
	}
	
	@Test
	public void post_fix_calcul() {
		Queue<String> pattern = new LinkedList<>();
		pattern.add("100");
		pattern.add("/");
		pattern.add("2");
		pattern.add("+");
		pattern.add("20");
		pattern.add("*");
		pattern.add("2");
		
		Calcul rpn = new Calcul();
		Queue<String> post = rpn.infixToPostfix(pattern);
		
		assertEquals(90.0, rpn.calculatePostfix(post), 0.001);
	}
}
