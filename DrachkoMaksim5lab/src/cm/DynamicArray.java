package com.company;

import java.util.Arrays;
import java.util.function.Function;

public class DynamicArray<T> {
    private T data[];
    private int size = 0;
    private static final int DEFAULT_CAPACITY = 9;

    public DynamicArray()
    {
        this(DEFAULT_CAPACITY);
    }

    @SuppressWarnings("unchecked")
    public DynamicArray(int capacity)
    {
        if(capacity < 0)
            throw new IllegalArgumentException();
        data = (T[]) new Object[capacity];
    }

    public void add(T item)
    {
        ensureCapacityIfNeeded();
        data[size++] = item;
    }

    public void remove(T item)
    {
        remove(indexOf(item));
    }

    public void remove(int index)
    {
        try{
        checkBoundInclusive(index);
        T removedItem = data[index];
        if(index != --size)
            System.arraycopy(data,index +1, data,index,size - index );
        data[size] = null;
        }catch (IndexOutOfRangeException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public T get(int index)
    {
        try{
            checkBoundInclusive(index);
            return (T) data[index];
        }catch (IndexOutOfRangeException ex)
        {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public boolean isExists(T item)
    {
        return indexOf(item) != -1;
    }

    public int indexOf(Object e)
    {
       for (int i = 0; i < size; i++) {
           if (e.equals(data[i]))
               return i;
       }

        return -1;
    }

    public T transform(int index, Function<T,T> function)
    {
        try {
            checkBoundInclusive(index);
            return function.apply(data[index]);
        }catch (IndexOutOfRangeException ex)
        {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    private void ensureCapacityIfNeeded()
    {
        if(size == data.length)
        {
            int newSize = data.length * 2;
            data = Arrays.copyOf(data,newSize);
        }
    }

    private void checkBoundInclusive(int index) throws IndexOutOfRangeException {
        if (index >= size || index < 0)
            throw new IndexOutOfRangeException("Index: " + index + ", Size: " + size);
    }
}
