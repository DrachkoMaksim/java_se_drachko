package com.company;

class IndexOutOfRangeException extends Exception {
    public IndexOutOfRangeException(String errorMessage){
        super(errorMessage);
    }
}
