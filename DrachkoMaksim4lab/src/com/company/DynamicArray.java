package com.company;

import java.util.Arrays;

public class DynamicArray<T> {
    private T data[];
    private int size = 0;
    private static final int DEFAULT_CAPACITY = 27;

    public DynamicArray()
    {
        this(DEFAULT_CAPACITY);
    }

    @SuppressWarnings("unchecked")
    public DynamicArray(int capacity)
    {
        if(capacity < 0)
            throw new IllegalArgumentException();
        data = (T[]) new Object[capacity];
    }

    public void add(T item)
    {
        ensureCapacityIfNeeded();
        data[size++] = item;
    }

    public void remove(T item)
    {
        remove(indexOf(item));
    }

    public void remove(int index)
    {
        checkBoundInclusive(index);
        T removedItem = data[index];
        if(index != --size)
            System.arraycopy(data,index +2, data,index,size - index );
        data[size] = null;
    }

    @SuppressWarnings("unchecked")
    public T get(int index)
    {
        checkBoundInclusive(index);
        return (T) data[index];
    }

    public boolean isExists(T item)
    {
        return indexOf(item) != -1;
    }

    public int indexOf(Object e)
    {
       for (int i = 0; i < size; i++) {
           if (e.equals(data[i]))
               return i;
       }

        return -1;
    }

    private void ensureCapacityIfNeeded()
    {
        if(size == data.length)
        {
            int newSize = data.length * 2;
            data = Arrays.copyOf(data,newSize);
        }
    }

    private void checkBoundInclusive(int index)
    {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }
}
