package com.company;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface NotNull {
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface IntMinMax {
    int min();
    int max();
}

class Student {
    @NotNull
    private String name;

    @IntMinMax(min =1,max = 9)
    private  int number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student(String name) {
        super();
        this.name = name;
    }

}

class ModelIsNotValidException extends RuntimeException {
    public ModelIsNotValidException(String errorMessage) {
        super(errorMessage);
    }
}

public class Main {

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
        Student s = new Student("A");

        try {
            for(Field f : s.getClass().getDeclaredFields()) {
                if (f.isAnnotationPresent(NotNull.class)) {
                    if (f.getType() != String.class)
                        continue;
                    NotNull notNull = f.getAnnotation(NotNull.class);

                    f.setAccessible(true);
                    String value =(String) f.get(s);
                    if (value == null)
                        throw new NullPointerException();
                }
                if (f.isAnnotationPresent(IntMinMax.class)) {
                    if (f.getType() != String.class)
                        continue;
                    IntMinMax strMinMax = f.getAnnotation(IntMinMax.class);
                    f.setAccessible(true);
                    int value =(int) f.get(s);
                    if (value < strMinMax.min() || value > strMinMax.max())
                        throw new ModelIsNotValidException("String max min validation not passed");
                }

            }

        }
        catch(ModelIsNotValidException e ) {
            System.out.println("Model is not valid: " + e.getMessage());
        }
        catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }

    }
}
