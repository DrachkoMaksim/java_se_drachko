
public class StringHelper {
	public static String removeCharacter(String source,String removable)
	{
		return source.replace(removable,"");
	}
	
	public static boolean isNumeric(String str)
	{
		return str.matches("-?\\d+(\\.\\d+)?");
	}
	
	public static boolean checkForRepeatability(String str)
	{
		for(int i = 0; i < str.length() - 1; i++)
		{
			if(str.charAt(i) == str.charAt(i+1))
				return true;
		}
		
		return false;
	}
	
	public static String buildHugeString()
	{
		int length = 2000;
		int counter = 0;
		StringBuffer output = new StringBuffer(length);
		
		for(int i = 0;i < length; i++)
			output.append(i);
		
		return output.toString();
	}
}
